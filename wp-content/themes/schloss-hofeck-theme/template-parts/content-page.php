<div id="top-content">
</div>
<div id="wrapper">
        <?php
        the_content();
        ?>
</div>
<div id="anfahrt">
            <div id="map"></div>
                <script>
                var map;
                function initMap() {
                    map = new google.maps.Map(document.getElementById('map'), {
                        center: {lat:50.329123,lng:11.8917831},
                        zoom: 17,                
                        styles:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f9f4f0"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"lightness":17},{"color":"#e9e6e3"}]}]
                    });
                    
                    var iconBase = 'https://www.schloss-hofeck.de/wp-content/themes/schloss-hofeck-theme/dist/images/';
                    var marker = new google.maps.Marker({
                        position:  {lat: 50.329123,lng:11.8917831 } ,
                        map: map,
                        icon: iconBase + 'pin.png'
                    });    
                }
                </script>
                <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCZNX4sSI6e3vEDI-RBT1tJhC-c1ZdL1Bs&callback=initMap"
                async defer>
                </script>
        </div>      
</div>
