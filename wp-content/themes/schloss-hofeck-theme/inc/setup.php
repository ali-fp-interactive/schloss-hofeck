<?php

if (!function_exists('schloss_hofeck_theme_setup')) {
	function schloss_hofeck_theme_setup()
	{
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');

		register_nav_menus(array(
			'main-menu' => esc_html__('Hauptmenü'),
			'footer-menu' => esc_html__('Footermenü')
		));

		add_theme_support('customize-selective-refresh-widgets');
	}
}
add_action('after_setup_theme', 'schloss_hofeck_theme_setup');

//Register Custom Slide Types
function slides_post_type()
{
	register_post_type(
		'slides',
		array(
			'labels' => array(
				'name' => __('Slides'),
				'singular_name' => __('Slide')
			),
			'public' => true,
			'publicly_queryable' => false,
			'menu_icon' => 'dashicons-slides',
			'supports' => array('title', 'thumbnail'),
			'has_archive' => true,
		)
	);
	//Registering Slides Catagory
	register_taxonomy('slider', 'slides', array(
		'labels' => array(
		  'name' => __('Slider'),
		  'singular_name' => __('Slider'),
		  'add_new_item' => __('Neuen Slider erstellen'),
		  'parent_item' => __('Übergeordneter Slider')
		),
		'show_admin_column' => true,
		'hierarchical' => true
	  ));

}
add_action('init', 'slides_post_type');
add_filter('show_admin_bar', '__return_false');
 