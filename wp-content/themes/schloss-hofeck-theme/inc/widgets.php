<?php

function schloss_hofeck_theme_widgets_init() {
	register_sidebar(array(
		'name' => esc_html__('Sidebar'),
		'id' => 'sidebar',
	));
}
add_action('widgets_init', 'schloss_hofeck_theme_widgets_init');
