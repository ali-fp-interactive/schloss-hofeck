<?php
/*
Element Description: VC Info Box
 */

// Element Class
class slider extends WPBakeryShortCode
{
    // Element Init
    function __construct()
    {
        add_action('init', array($this, 'slider_mapping'));
        add_shortcode('slider', array($this, 'slider_html'));
    }

    // Element Mapping
    public function slider_mapping()
    {

           // Stop all if VC is not enabled
        if (!defined('WPB_VC_VERSION')) {
            return;
        }
            // Map the block with vc_map()
        vc_map(
            array(
                'name' => __('Slider'),
                'base' => 'slider',
                'description' => __('Slider Element'),
                'category' => __('My Custom Elements'),
                'params' => array(

                    array(
                        'type' => 'textfield',
                        'heading' => __('Slider (slug)'),
                        'param_name' => 'slider'
                    ),

                    array(
                        'type' => 'dropdown',
                        'heading' => __('Layout'),
                        'param_name' => 'layout',
                        'value' => array(
                            __('Bild links') => 'image-left',
                            __('Bild rechts') => 'image-right'
                        ),
                    )

                )
            )
        );

    }


    // General Image Slider HTML
    public function slider_html($atts)
    {
        //Left Image slider
        $atts = shortcode_atts(array(
            'slider' => '',
            'layout' => 'image-left',
        ), $atts);
        ?>
        <div class="slider <?php echo $atts['layout'] /*calling the array to show the key value*/ ?>">
        <?php
            $args = array('post_type' => 'slides', 'slider' => $atts['slider'] );
            $loop = new WP_Query( $args );  /* Generalised calling of the slides to be used inside the loop */

            ?>
            <div class="image-slider">
            <?php
                while($loop->have_posts()) : $loop->the_post();
                    if (has_post_thumbnail() )
                    {
                        $large_image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'large' );
                    }
                    ?>
                        <div class="images"><img src="<?php echo $large_image_url[0]; ?>"></div>
                    <?php
                endwhile;
            ?>
            </div><!--

         --><div class="text-bg-slider"><!--  the background COLOR for the image behind the text on the slider -->
                <?php
                    while($loop->have_posts()) : $loop->the_post();
                        $bg_color = get_field('bg-color');
                        ?>
                            <div class="text-bg-slide" data-test="<?php echo $bg_color; ?>" style="background-image: url('<?php echo wp_get_attachment_image_src($bg_color, 'large')[0];  ?>')"></div>
                        <?php
                    endwhile;
                ?>
            </div>
                <div class="text-slider">
                  <div class="slides">
                    <?php
                            while($loop->have_posts()) : $loop->the_post();
                                $slide_title = get_field('ueberschrift');
                                $slide_text = get_field('text');
                                ?>
                                    <div class="slide" data-arrow="<?php echo get_field('pfeil-box-farbe'); ?>">
                                        <div class="slider-heading" style="border-color:<?php echo get_field('text-border-farbe')?> !important">
                                            <h3>
                                                <?php
                                                    echo add_shy($slide_title);
                                                ?>
                                            </h3>
                                        </div>
                                        <div class="slider-details" style="border-color:<?php echo get_field('text-border-farbe')?> !important">
                                            <p>
                                                <?php
                                                    echo $slide_text;

                                                ?>
                                            </p>
                                        </div><!--
                                    -->
                                        <div class="slide-bullets <?php echo get_field('slide-bullet-farbe'); ?>" >
                                                <?php
                                                    $slide_bullets = get_field('slide-bullets');
                                                    echo $slide_bullets;
                                                ?>
                                        </div>
                                        <?php /* <div class="slide-nav">
                                            <?php
                                                    $pfeil_box_farbe=get_field('pfeil-box-farbe');
                                                    switch($pfeil_box_farbe){
                                                        case 'tuerkis':
                                                            echo '<div class="previous"><img src="' . get_template_directory_uri() . '/dist/images/tuerkis-pfeil-links.png"></div><!--
                                                                --><div class="next"><img src="' . get_template_directory_uri() . '/dist/images/tuerkis-pfeil-rechts.png"></div>';
                                                                break;
                                                        case 'gold':
                                                            echo '<div class="previous"><img src="' . get_template_directory_uri() . '/dist/images/gold-pfeil-links.png"></div><!--
                                                                --><div class="next"><img src="' . get_template_directory_uri() . '/dist/images/gold-pfeil-rechts.png"></div>';
                                                            break;
                                                    }
                                            ?>
                                        </div> */ ?>
                                     </div>

                                <?php
                            endwhile;
                      ?>
                  </div>
                  <div class="arrows">
                    <div class="previous"><img class="gold" src="<?php echo get_template_directory_uri(); ?>/dist/images/gold-pfeil-links.png"><img class="tuerkis" src="<?php echo get_template_directory_uri(); ?>/dist/images/tuerkis-pfeil-links.png"></div>
                    <div class="next"><img class="gold" src="<?php echo get_template_directory_uri(); ?>/dist/images/gold-pfeil-rechts.png"><img class="tuerkis" src="<?php echo get_template_directory_uri(); ?>/dist/images/tuerkis-pfeil-rechts.png"></div>
                  </div>
                </div>
            </div>
        <?php

    }

} // End Element Class

// Element Class Init
new slider();
