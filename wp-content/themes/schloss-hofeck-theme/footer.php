			</div>
		
			<footer id="footer">
					<div id="kontakt">
						<div class="form-heading">
							<h3> KONTAKT </h3>
						</div><?php
						echo do_shortcode('[gravityforms id=2 title=false ajax=true]');
						?>
					</div>


			</footer>
			<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
			<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
			<script type="text/javascript" src="slick/slick.min.js"></script>
			<script type="text/javascript">
				$(document).ready(function(){
					$('images-slider').slick({
						infinite: true,
						slidesToshow: 1,
						slidesToScroll:1
					});
				});
		</script>
		</div>
		<div id="footer-menu">
			<?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
		</div>
		<?php wp_footer(); ?>


	</body>
</html>
