<?php
    $attachment_id = get_post_thumbnail_id();
    $image = wp_get_attachment_image_src($attachment_id,'large');
    $image_url = $image[0];
    $image_name = explode('.', wp_basename(get_attached_file($attachment_id)))[0];
    $videos_path =  dirname(dirname(__FILE__)) . '/videos/';
    if(strpos($image_name, 'video_') === 0 AND file_exists($videos_path . str_replace('video_', '', $image_name) . '.mp4')) {
        $has_video = true;
    } else {
        $has_video = false;
    }
?>
<div class="headervideo<?php if($has_video == false) { echo ' image'; } ?>" style="background-image: url('<?php echo $image_url; ?>')">
    <?php
        if($has_video == true) {
            $video_file = str_replace('video_', '', $image_name) . '.mp4';
            $video_file_webm = str_replace('video_', '', $image_name) . '.webm';
            ?>
                <video id="video" class="video" poster="<?php echo $image_url; ?>" loop muted playsinline >
                    <?php /* <source src="<?php echo get_template_directory_uri() . '/videos/'.  $video_file_webm; ?>" type="video/webm"></source> */ ?>
                   <source src="<?php echo get_template_directory_uri() . '/videos/'.  $video_file; ?>?v=2" type="video/mp4"> 
                </video>
            <?php
        }
    ?>
    <div class="scroll-down">
        <div class="icon"></div>
        <span class="label">Scrollen</span>
    </div>
</div>
