<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<title>Schloss Hofeck</title>
		<link href="">
		<meta charset="<?php bloginfo('charset'); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="google-site-verification" content="tnMbvQxsfI77f61TMf3hX6sb5MayUxs9a7xYKzG7XUQ" />
		<?php wp_head(); ?>
	</head>
	<body id="body" <?php body_class(); ?>>
		<div id="page">
			<header id="header">
				<div class="menu">
					<div class="iconbox">
						<div class="icon"></div>
						<div class="seperator"></div>
						<div class="hamburger">
							<span></span>
							<span></span><span></span>
							<span></span>
						</div>
						<div class="anfragen" ><a href="#kontakt">Anfragen</a> </div>
					</div>

					<div class="blur-menu"></div>
					<ul class="menu-items">
						<?php
							$nav_menu_locations = get_nav_menu_locations();
							$nav_menu = get_term($nav_menu_locations['main-menu'], 'nav_menu');
							$items = wp_get_nav_menu_items($nav_menu->slug);
							if(!empty($items) && count($items) > 0) {
							?>
						<nav class="navigation">
							<li><a href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/dist/images/logo-schloss-hofeck.png"></a></li>
							<?php
								foreach($items as $item) {
									?>
										<li><a href="<?php echo $item->url; ?>"><?php echo $item->title; ?></a></li>
									<?php
								}
							?>
						</nav>
								<?php
							}
						?>
					</ul>	
				</div>
			</header>
			<div id="content">
