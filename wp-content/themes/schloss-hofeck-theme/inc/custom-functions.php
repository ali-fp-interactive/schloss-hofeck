<?php

function add_shy($string) {
    $string = str_ireplace('Kochevents', 'Koch&shy;events', $string);
    $string = str_ireplace('Schwimmbad', 'Schwimm&shy;bad', $string);
    return $string;
}