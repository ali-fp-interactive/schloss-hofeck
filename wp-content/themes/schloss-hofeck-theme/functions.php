<?php

require get_template_directory() . '/inc/setup.php';
require get_template_directory() . '/inc/admin.php';
require get_template_directory() . '/inc/widgets.php';
require get_template_directory() . '/inc/clean-up.php';
require get_template_directory() . '/inc/scripts.php';
require get_template_directory() . '/inc/custom-functions.php';

/* wp_enqueue_script( 'smoothup', get_template_directory_uri() . '/src/scripts/smoothscroll.js', array( 'jquery' ), '', true ); */