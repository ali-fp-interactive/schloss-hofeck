(function() {

    objectFitImages();
    var textSlider = $('.text-slider .slides');
    var imageSlider = $('.image-slider');
    var textBgSlider = $('.text-bg-slider');

    textSlider.each(function() {
        $(this).slick({
            slidesToShow: 1,
            fade: true,
            cssEasy: 'linear',
            arrows: false,
            asNavFor: '#' + $(this).closest('[id]').attr('id') + ' .image-slider, #' + $(this).closest('[id]').attr('id') + ' .text-bg-slider',
            autoplay: true,
            autoplaySpeed: 5000,
            waitForAnimate: false //to debug the slide transition time delay when the menu item is clicked, which also takes the slider to the first slide
        });
        $(this).on('init', function(event, slick) {
            var thisEl = $(this);
            setTimeout(function() {
                var arrow = thisEl.find('.slick-slide.slick-active .slide').data('arrow');
                thisEl.parent().find('.arrows img').stop().fadeOut(250);
                thisEl.parent().find('.arrows img.' + arrow).stop().fadeIn(250);
            }, 1);
        });
        $(this).on('beforeChange', function(event, slick, currentSlide) {
            var thisEl = $(this);
            setTimeout(function() {
                var arrow = thisEl.find('.slick-slide.slick-active .slide').data('arrow');

                thisEl.parent().find('.arrows img').stop().fadeOut(250);
                thisEl.parent().find('.arrows img.' + arrow).stop().fadeIn(250);
            }, 1);
        });
        $(this).parent().parent().find('.image-slider').slick({
            slidesToShow: 1,
            fade: true,
            cssEasy: 'linear',
            arrows: false,
            waitForAnimate: false
        });
        $(this).parent().parent().parent().find('.text-bg-slider').slick({
            slidesToShow: 1,
            fade: true,
            cssEasy: 'linear',
            arrows: false,
            waitForAnimate: false
        });
    });

    $(".previous").click(function() {
        textSlider.slick('slickPrev');

    });
    $(".next").click(function() {
        textSlider.slick('slickNext');
    });
    if (window.location.hash) {
        var offset = $(window.location.hash).offset().top;
        if ($(window.location.hash).length) {
            //we notify jquery with  # that we need that part, then we take the href part and split the linnk so we can attach it later.
            // the array [0] is the link and the array [1] is the part after the #
            $("html, body").animate({
                scrollTop: offset - 168
            }, 1000);
            console.log('test1');

        }
    }

    $('a[href*="#"]').click(function(e) {

        var offset = $('#' + $(this).attr('href').split('#')[1]).offset().top;
        if ($('#' + $(this).attr('href').split('#')[1]).length) {
            e.preventDefault(); //we notify jquery with  # that we need that part, then we take the href part and split the linnk so we can attach it later.
            // the array [0] is the link and the array [1] is the part after the #
            $("html, body").animate({
                scrollTop: offset - 168
            }, 1000);

        }


        textSlider.slick('slickGoTo', 1);
        textSlider.slick('slickGoTo', 0);
    });




    $('.scroll-down').click(function() {
        $("html, body").animate({
            scrollTop: $(window).height() - 109
        }, 1000);
    });


    $("div.hamburger").click(function() {
        $(this).toggleClass("hamburger-expanded");
        if ($(".menu-items").is(":hidden")) {
            $(".menu-items").slideDown("slow"); //show burger menu
        } else  {
            $(".menu-items").slideUp("slow"); //hide burger menu on click of the burger icon
        }
    });
    if ($(window).width() < 981) {

        $(".navigation li").click(function() {

            $("div.hamburger").removeClass("hamburger-expanded");
            $(".menu-items").slideUp("slow"); //slide up after the menu click

        });
    }

    $(window).resize(function() {
        if ($(window).width() > 980 && $(".menu-items").is(":hidden")) {
            $(".menu-items").removeAttr('style');
        }
    });


    $('.gfield.datepicker input').datepicker({
        dateFormat: 'dd.mm.yy'
    });

    $('#video').get(0).play();

})();