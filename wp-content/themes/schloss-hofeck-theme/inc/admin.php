<?php

function schloss_hofeck_theme_admin() {
	remove_menu_page('edit.php');
	remove_menu_page('edit-comments.php');

	remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
	remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
	remove_meta_box('dashboard_primary', 'dashboard', 'side');
	remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
	remove_meta_box('dashboard_quick_press', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');
	remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');
	remove_meta_box('dashboard_right_now', 'dashboard', 'normal');
	remove_meta_box('dashboard_activity', 'dashboard', 'normal');
}
add_action('admin_init', 'schloss_hofeck_theme_admin');

function schloss_hofeck_theme_menu_order($menu_ord) {
	if(!$menu_ord) {
		return true;
	}

	return array(
		'index.php',
		'separator1',
		'upload.php',
		'edit.php?post_type=page',
		'separator2',
		'themes.php',
		'plugins.php',
		'users.php',
		'tools.php',
		'options-general.php',
		'separator-last',
	);
}
add_filter('custom_menu_order', 'schloss_hofeck_theme_menu_order');
add_filter('menu_order', 'schloss_hofeck_theme_menu_order');

function schloss_hofeck_theme_admin_bar_render() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('comments');
}
add_action('wp_before_admin_bar_render', 'schloss_hofeck_theme_admin_bar_render');

function schloss_hofeck_theme_remove_comment_support() {
	remove_post_type_support('post', 'comments');
	remove_post_type_support('page', 'comments');
}
add_action('init', 'schloss_hofeck_theme_remove_comment_support', 100);

function schloss_hofeck_theme_remove_css_section($wp_customize) {
	$wp_customize->remove_section('custom_css');
}
add_action('customize_register', 'schloss_hofeck_theme_remove_css_section', 15);

// Before VC Init - Add new ELEMENT here ////////
add_action( 'vc_before_init', 'vc_before_init_actions' );
 
function vc_before_init_actions() {
     
    //.. Code from other Tutorials ..//
 
    // Require new custom Element
    require_once( get_template_directory().'/vc-elements/slider.php' ); 
     
}


/**
 * Remove the Color Picker plugin from tinyMCE. This will
 * prevent users from adding custom colors. Note, the default color
 * palette is still available (and customizable by developers) via
 * textcolor_map using the tiny_mce_before_init hook.
 * 
 * @param array $plugins An array of default TinyMCE plugins.
 */
add_filter( 'tiny_mce_plugins', 'wpse_tiny_mce_remove_custom_colors' );
function wpse_tiny_mce_remove_custom_colors( $plugins ) {   
    foreach ( $plugins as $key => $plugin_name ) {
        if ( 'colorpicker' === $plugin_name ) {
            unset( $plugins[ $key ] );
            return $plugins;            
        }
    }
    return $plugins;            
}

/* TimyMCE Colorpicker Predefined Color Option Enabler */
function mytheme_change_tinymce_colors( $init ) {

    $custom_colours = '
        "57B6B2", "Tuerkis",
        "B1A06E", "Gold",
        "FFFFFF", "White",
        "20362C", "Schwarz",
        "B43530", "Rot",
        "20362C", "Gruen",
        ';
    $init['textcolor_map'] = '['.$custom_colours.']';
    $init['textcolor_rows'] = 1; // expand colour grid to 6 rows
    return $init;
}
add_filter('tiny_mce_before_init', 'mytheme_change_tinymce_colors');
