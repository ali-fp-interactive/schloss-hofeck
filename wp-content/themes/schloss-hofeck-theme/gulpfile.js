var gulp = require('gulp'),
    imagemin = require('gulp-imagemin'),
    jshint = require('gulp-jshint'),
    minify = require('gulp-minify'),
    minifycss = require('gulp-minify-css'),
    rename = require('gulp-rename'),
    sass = require('gulp-sass'),
    watch = require('gulp-watch');
var browserSync = require('browser-sync').create();

gulp.task('sass', function() {
    gulp.src('./src/styles/*.scss')
        .pipe(sass({ outputStyle: 'expanded' }))
        .pipe(gulp.dest('./dist/styles'))
        .pipe(minifycss())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./dist/styles'))
        .pipe(browserSync.stream());
});

gulp.task('js', function() {
    gulp.src('./src/scripts/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(minify({
            ext: {
                src: '.js',
                min: '.min.js'
            }
        }))
        .pipe(gulp.dest('./dist/scripts'));
});

gulp.task('images', function() {
    gulp.src('./src/images/**/*')
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/images'));
});

gulp.task('fonts', function() {
    gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('watch', function() {
    browserSync.init({
        proxy: "schloss-hofeck.localhost"
    });
    gulp.watch('src/styles/**/*.scss', ['sass']);
    gulp.watch('src/scripts/*.js', ['js']).on('change', browserSync.reload);
    gulp.watch('*.php').on('change', browserSync.reload);
    gulp.watch('inc/*.php').on('change', browserSync.reload);
    gulp.watch('template-parts/*.php').on('change', browserSync.reload);
});

gulp.task('default', ['sass', 'js', 'images', 'fonts']);