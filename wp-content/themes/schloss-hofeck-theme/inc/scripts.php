<?php

function schloss_hofeck_theme_scripts() {
	wp_enqueue_style('schloss_hofeck_theme-style', get_template_directory_uri() . '/dist/styles/main.min.css');
	wp_enqueue_script('schloss_hofeck_theme-jquery', get_template_directory_uri() . '/dist/scripts/jquery.min.js', array(), false, true);
	wp_enqueue_script('schloss_hofeck_theme-jquery-ui', get_template_directory_uri() . '/dist/scripts/jquery-ui.min.js', array(), false, true);
	wp_enqueue_script('schloss_hofeck_theme-ofi', get_template_directory_uri() . '/dist/scripts/ofi.min.js', array(), false, true);
	wp_enqueue_script('schloss_hofeck_theme-slick', get_template_directory_uri() . '/dist/scripts/slick.min.js', array(), false, true);
	wp_enqueue_script('schloss_hofeck_theme-main', get_template_directory_uri() . '/dist/scripts/main.min.js', array(), false, true);
}
add_action('wp_enqueue_scripts', 'schloss_hofeck_theme_scripts');

